<?php
/**
* WPTVIDEO_Customizer Class
*
* @package  WPTVIDEO
*/

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

if ( ! class_exists( 'WPTVIDEO_Customizer' ) ) :

  /**
  * The WPTVIDEO_Customizer class
  */
  class WPTVIDEO_Customizer {

    /**
    * Setup class.
    */
    public function __construct() {
      add_action( 'customize_register', array( $this, 'wptvideo_customize_register' ));
      add_action( 'customize_controls_enqueue_scripts', array( $this, 'wptvideo_customize_controls_scripts' ) );
    }

    /**
    * Add script for customize controls
    */
    public function woostify_customize_controls_scripts() {
      // wp_enqueue_script(
      // 	'woostify-condition-control',
      // 	WOOSTIFY_THEME_URI . 'inc/customizer/custom-controls/conditional/js/condition.js',
      // 	array(),
      // 	woostify_version(),
      // 	true
      // );
      //
      // wp_enqueue_script(
      // 	'woostify-condition-control',
      // 	WOOSTIFY_THEME_URI . 'inc/customizer/custom-controls/conditional/js/condition.js',
      // 	array(),
      // 	woostify_version(),
      // 	true
      // );
    }

    public function wptvideo_customize_register($wp_customize){
        //get_theme_mod('test_text_setting')
        //adding section in wordpress customizer
        $wp_customize->add_section('footer_settings_section', array(
          'title'          => __('동영상옵션','wptvideo')
        ));
        //adding setting for footer text area
        $wp_customize->add_setting('test_text_setting', array(
          'default'        => 'Default Text For Footer Section',
        ));
        $wp_customize->add_control('test_text_setting', array(
          'label'   => 'Footer Text Here',
          'section' => 'footer_settings_section',
          'type'    => 'textarea',
        ));

      }

  }

  endif;

  return new WPTVIDEO_Customizer();

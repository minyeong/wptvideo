<?php


class WPTVIDEO_Single_Video
{

  function __construct()
  {
    $this->init_hooks();
  }

  private function init_hooks(){
    add_filter('single_template', array($this,'load_video_template'), 10, 1);
    add_action( 'wptvideo_sidebar', array($this,'get_sidebar'), 10, 1 );
  }

  public function load_video_template($template) {
    global $post;
    $template = $this->get_current_template();
    if ($post->post_type == "vimeo-video" && $template !== locate_template(array("single-vimeo-video.php"))){
      return WPTVIDEO_ABSPATH . "templates/".$template."/single-vimeo-video.php";
    }

    return $template;
  }

  public function get_current_template(){
    $theme = $this->get_current_theme();
    if ($theme == 'Woostify') {
      $template = 'default';
    }else {
      $template = 'default';
    }
    return $template;
  }

  public function get_current_theme(){
    $theme = wp_get_theme();
    if ($theme) {
      return $theme->name;
    }
    return '';
  }

  public function get_sidebar($template) {
    $template = $this->get_current_template();
    if (locate_template(array("sidebar-vimeo-video.php"))) {
      get_sidebar('vimeo-video');
      return;
    }

    if ( is_singular( 'product' ) ) {
      get_sidebar( 'shop' );
    } else {
      $this->get_custom_sidebar($template);
    }
  }

  public function get_custom_sidebar($template){
    include_once WPTVIDEO_ABSPATH . "templates/".$template."/sidebars/".$this->get_sidebar_type().".php";
  }

  public function get_sidebar_type(){
    return 'default';
  }

}

return new WPTVIDEO_Single_Video();

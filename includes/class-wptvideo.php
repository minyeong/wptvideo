<?php
/**
 * WPTVIDEO setup
 *
 * @package WPTVIDEO
 * @since   1.0.0
 */

defined( 'ABSPATH' ) || exit;


final class WPTVIDEO {

	public $version = '1.0.0';
	protected static $_instance = null;
	public $session = null;
	public $single_post = null;
	public $integrations = null;
	public $cart = null;
	public $customer = null;
	public $order_factory = null;
	public $structured_data = null;
	public $deprecated_hook_handlers = array();

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __construct() {
		$this->define_constants();
		$this->includes();
		$this->init_hooks();

		do_action( 'wptvideo_loaded' );
	}

	private function init_hooks() {
		register_activation_hook( WPTVIDEO_PLUGIN_FILE, array( 'wptvideo_Install', 'install' ) );
		// register_shutdown_function( array( $this, 'log_errors' ) );
		add_action( 'after_setup_theme', array( $this, 'setup_environment' ) );
		add_action( 'after_setup_theme', array( $this, 'include_template_functions' ), 11 );
		add_action( 'init',array($this,'init'));
	}

	// public function log_errors() {
	// 	$error = error_get_last();
	// 	if ( in_array( $error['type'], array( E_ERROR, E_PARSE, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR ) ) ) {
	// 		$logger = wc_get_logger();
	// 		$logger->critical(
	// 			/* translators: 1: error message 2: file name and path 3: line number */
	// 			sprintf( __( '%1$s in %2$s on line %3$s', 'wpteam-video' ), $error['message'], $error['file'], $error['line'] ) . PHP_EOL,
	// 			array(
	// 				'source' => 'fatal-errors',
	// 			)
	// 		);
	// 		do_action( 'wptvideo_shutdown_error', $error );
	// 	}
	// }

	/**
	 * Define WC Constants.
	 */
	private function define_constants() {
		$upload_dir = wp_upload_dir( null, false );

		$this->define( 'WPTVIDEO_ABSPATH', dirname( WPTVIDEO_PLUGIN_FILE ) . '/' );
		$this->define( 'WPTVIDEO_PLUGIN_BASENAME', plugin_basename( WPTVIDEO_PLUGIN_FILE ) );
		$this->define( 'WPTVIDEO_VERSION', $this->version );
		$this->define( 'WPTVIDEO_LOG_DIR', $upload_dir['basedir'] . '/wptvideo-logs/' );
		$this->define( 'WC_TEMPLATE_DEBUG_MODE', false );
	}

	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	private function is_request( $type ) {
		switch ( $type ) {
			case 'admin':
				return is_admin();
			case 'ajax':
				return defined( 'DOING_AJAX' );
			case 'cron':
				return defined( 'DOING_CRON' );
			case 'frontend':
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' ) && ! defined( 'REST_REQUEST' );
		}
	}

	public function includes() {
		include_once WPTVIDEO_ABSPATH . 'includes/class-wptvideo-customizer.php';
		include_once WPTVIDEO_ABSPATH . 'includes/class-wptvideo-single-video.php';
	}

	public function include_template_functions() {
		include_once WPTVIDEO_ABSPATH . 'includes/wptvideo-template-functions.php';
	}

	/**
	 * Init WooCommerce when WordPress Initialises.
	 */
	public function init() {
		// Before init action.
		do_action( 'before_wptvideo_init' );

		// Set up localisation.
		$this->load_plugin_textdomain();

		// Load class instances.
		// $this->single_post = new WPTVIDEO_Single_Video();
		if ( $this->is_request( 'frontend' ) ) {

		}

		// Init action.
		do_action( 'wptvideo_init' );
	}

	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
	 *
	 * Locales found in:
	 *      - WP_LANG_DIR/woocommerce/woocommerce-LOCALE.mo
	 *      - WP_LANG_DIR/plugins/woocommerce-LOCALE.mo
	 */
	public function load_plugin_textdomain() {
		$locale = is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
		$locale = apply_filters( 'plugin_locale', $locale, 'wptvideo' );

		unload_textdomain( 'wptvideo' );
		load_textdomain( 'wptvideo', WP_LANG_DIR . '/wptvideo/wptvideo-' . $locale . '.mo' );
		load_plugin_textdomain( 'wptvideo', false, plugin_basename( dirname( WPTVIDEO_PLUGIN_FILE ) ) . '/i18n/languages' );
	}

	public function setup_environment() {
		$this->define( 'WPTVIDEO_TEMPLATE_PATH', $this->template_path() );
	}

	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', WPTVIDEO_PLUGIN_FILE ) );
	}

	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( WPTVIDEO_PLUGIN_FILE ) );
	}

	public function template_path() {
		return apply_filters( 'wptvideo_template_path', 'wptvideo/' );
	}

	public function ajax_url() {
		return admin_url( 'admin-ajax.php', 'relative' );
	}

	// public function queue() {
	// 	return WC_Queue::instance();
	// }
}

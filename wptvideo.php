<?php
/**
* Plugin Name: WPTVIDEO
* Plugin URI: http://wpteam.kr
* Description: video post with woocomerce
* Version: 1.0.0
* Author: wpteam
* Author URI:  http://wpteam.kr
* Text Domain: wptvideo
*
* @package WPTVIDEO
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define WPT_VIDEO_FILE.
if ( ! defined( 'WPTVIDEO_PLUGIN_FILE' ) ) {
	define( 'WPTVIDEO_PLUGIN_FILE', __FILE__ );
}

if (! class_exists('CVM_Vimeo_Videos') ) {
	function sample_admin_notice__success() {
		if ( file_exists( plugin_dir_path( __DIR__ ).'/vimeo-video-post/main.php' ) ) {
			$path = '/vimeo-video-post/main.php';
			$text = 'WPTVIDEO 플러긴은 Vimeotheque을 필수로 필요로 합니다. 활성화 이전에 해당 플러긴을 활성화 해주시길 바랍니다.';
			$url = wp_nonce_url(admin_url('plugins.php?action=activate&plugin='.$path), 'activate-plugin_'.$path);
			$btn_text = '활성화';
		}else {
			$action = 'install-plugin';
			$slug = 'codeflavors-vimeo-video-post-lite';
			$url =  wp_nonce_url(
				add_query_arg(
					array(
						'action' => $action,
						'plugin' => $slug
					),
					admin_url( 'update.php' )
				),
				$action.'_'.$slug
			);
			$text = 'WPTVIDEO 플러긴은 Vimeotheque을 필수로 필요로 합니다. 활성화 이전에 해당 플러긴을 설치해주시길 바랍니다.';
			$btn_text = '설치하기';
		}
		?>
		<div class="notice notice-error is-dismissible">
			<p>
				<?php echo sprintf(
					__( '%s <a href="%s">%s</a>', 'wptvideo' ),
					$text, esc_url( $url ), $btn_text
				); ?>
			</p>
		</div>
		<?php
	}
	add_action( 'admin_notices', 'sample_admin_notice__success' );
}

// Include the main WPTVIDEO class.
if ( ! class_exists( 'WPTVIDEO' ) ) {
	include_once dirname( __FILE__ ) . '/includes/class-wptvideo.php';
}

/**
* Main instance of WPTVIDEO.
*
*
* @since  2.1
* @return WPTVIDEO
*/
function wpt() {
	return WPTVIDEO::instance();
}

// Global for backwards compatibility.
$GLOBALS['wptvideo'] = wpt();

<?php

get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();

				do_action( 'wptvideo_post_before' );

				get_template_part( 'template-parts/content', 'page' );

				do_action( 'wptvideo_post_after' );

			endwhile;
			?>
		</main>
	</div>

<?php
do_action( 'wptvideo_sidebar', 'default' );

get_footer();
